## Climate forecasting using ANN

ANN-FC uses FANN (Fast Artificial Neural Networks) to predict climate (and weather, if required) based on data by forecast.io. Code is written in PHP, it is still in rudimentary phase, but works.

### Requirements
1. PHP v5.6+
2. Composer dependency managager
3. Apache/NGINX/lighthttpd/etc...

### License

**GPL 3.0**

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.