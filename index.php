<?php

require_once 'ANN/Loader.php';
require_once 'vendor/autoload.php';
 
use ANN\Network;
use ANN\Values;
 
try
{
  $objNetwork = Network::loadFromFile('dt.dat');
}
catch(Exception $e)
{
  print 'Creating a new one...';
 
  $objNetwork = new Network;
 
  $objValues = new Values;
 
  $objValues->train()
            ->input(0,0)->output(0)
            ->input(0,1)->output(1)
            ->input(1,0)->output(1)
            ->input(1,1)->output(0);
 
  $objValues->saveToFile('values_dt.dat');
 
  unset($objValues);
}
 
try
{
  $objValues = Values::loadFromFile('values_dt.dat');
}
catch(Exception $e)
{
  die('Loading of values failed');
}
 
$objNetwork->setValues($objValues); // to be called as of version 2.0.6
 
$boolTrained = $objNetwork->train();
 
print ($boolTrained)
        ? 'Network trained'
        : 'Network not trained completely. Please re-run the script';
 
$objNetwork->saveToFile('dt.dat');
 
$objNetwork->printNetwork();




// -------------------------------------------------------------------------------------------------------


try
{
  $objNetwork = Network::loadFromFile('dt.dat');
}
catch(Exception $e)
{
  die('Network not found');
}
 
try
{
  $objValues = Values::loadFromFile('values_dt.dat');
}
catch(Exception $e)
{
  die('Loading of values failed');
}
 
$objValues->input(0, 1)  // input values appending the loaded ones
          ->input(1, 1)
          ->input(1, 0)
          ->input(0, 0)
          ->input(0, 1)
          ->input(1, 1);
 
$objNetwork->setValues($objValues);

echo "<div style='clear:both;'></div><br/><br/><br/><pre>";
print_r($objNetwork->getOutputs());

?>